FROM maven
ENV  DEVO_GROUPNAME krypton
COPY SimpleAPI /home/krypton/SimpleAPI
ENTRYPOINT cd /home/krypton/SimpleAPI && mvn clean compile exec:java -PcheckstyleSkip
