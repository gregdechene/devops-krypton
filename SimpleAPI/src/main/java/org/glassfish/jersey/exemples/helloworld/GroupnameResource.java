
package org.glassfish.jersey.exemples.helloworld;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import com.google.gson.Gson;

@Path("/v1")
public class GroupnameResource {
    public static final String CLICHED_MESSAGE = "Hello World!";

    @GET
    @Path("/hello")
    @Produces("text/plain")
    public String getHello() {
        return CLICHED_MESSAGE;
    }

    @GET
    @Path("/groupname")
    @Produces("application/json")
    public String getResponseInJSON() {

        final String groupName = System.getenv("DEVO_GROUPNAME");
        Response response = new Response();

        response.setStatus("success");

        Data data = new Data();
        if (groupName != null && !groupName.isEmpty())
            data.setGroupname(groupName);
		else
	    data.setGroupname("");
        response.setData(data);

        String json = new Gson().toJson(response);
        return json;
    }
}
