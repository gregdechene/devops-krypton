package org.glassfish.jersey.exemples.helloworld;

/**
 * Created by Guillaume on 08/09/2016.
 */

public class Response {
    String status;
    Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
