package org.glassfish.jersey.exemples.helloworld;

/**
 * Created by Guillaume on 08/09/2016.
 */
public class Data {

    String groupname;

    public String getGroupname() {
            return groupname;
        }

    public void setGroupname(String groupname) {
            this.groupname = groupname;
        }
}
